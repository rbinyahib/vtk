## vtkPlotHistogram2D for n-components arrays

You can now set an array name for the `vtkPlotHistogram2D`.
This allows you to set an array that is not scalar, ie. an
array with a number of components greater than 1. It supports
displaying the array's magnitude values or component-wise
values.

You can also display the correct label values in the tooltip,
that has been fixed.
